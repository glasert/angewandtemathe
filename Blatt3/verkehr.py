import numpy as np
from numpy import pi, sin
import scipy.linalg
import matplotlib.pyplot as plt

A = np.array([  [-1, 0, 0, 0],
                [ 1,-1, 0, 0],
                [ 0, 1,-1, 0],
                [ 0,-2, 0, 1]])

L = np.identity(4)
U = A

param = np.array([1,1,-2])

for i in range(len(A)):
    U[1,i] += U[0,i]*param[0]
    U[2,i] += U[0,i]*param[1]
    U[3,i] += U[0,i]*param[2]

L[1,0] = -1*param[0]
L[2,1] = -1*param[1]
L[3,1] = -1*param[2]

print("L:")
print(L)
print("U:")
print(U)



x1 = []
x2 = []
x3 = []
x4 = []
time  = []

for t in range(0, 24*60): # 1440 Minuten
    t = t/60 # zu 24 Stunden
    
    zA = 610 - 100 * sin((pi / 12) * t    )
    aA = 450 -  25 * sin((pi /  3) * t + 5)
    zB = 400 -  20 * sin((pi /  3) * t - 1)
    aB = 640 +  50 * sin((pi / 12) * t - 7)
    zC = 600 +  10 * sin((pi /  6) * t + 4)
    
    b = np.array([aA-zA, aB-zB, -zC, 0])
    
    y = scipy.linalg.solve(L, b)
    x = scipy.linalg.solve(U, y)
    
    x1.append(x[0])
    x2.append(x[1])
    x3.append(x[2])
    x4.append(x[3])
    time.append(t)



fig, ax = plt.subplots()
ax.plot(time, x1, label='x1')
ax.plot(time, x2, label='x2')
ax.plot(time, x3, label='x3')
ax.plot(time, x4, label='x4')
ax.plot([0,24], [0,0], label='Nulllinie', color='black', linewidth=0.5)
ax.set_xlabel('Zeit [h]')
ax.set_ylabel('Flussmenge')
ax.legend()
plt.show()