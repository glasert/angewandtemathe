

Ergebnis:  
![Plot](Diagramm.png)

Alle Straßen außer x2 können in Einbahnstraßen umgewandelt werden, da der Fluss immer positiv bzw. negativ bleibt.
Bei x2 wechselt der Fluss zweimal am Tag die Richtung (Vorzeichenwechsel).


L: [  
 [ 1.  0.  0.  0.]  
 [-1.  1.  0.  0.]  
 [ 0. -1.  1.  0.]  
 [ 0.  2.  0.  1.]]

U: [  
 [-1  0  0  0]  
 [ 0 -1  0  0]  
 [-1  1 -1  0]  
 [ 2 -2  0  1]]