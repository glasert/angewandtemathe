# Rucksackproblem

## Brute Force
Für jedes Item wird rekursiv in den binären Baum abgestiegen.
Nach dem Prüfen auf Abbruchkonditionen und die beste Kombination
wird zuerst mit Bit 1 "nach links" und darauf mit Bit 0 "nach rechts" abgestiegen bzw. traversiert.
Die `traverse()` Funktion hat viele Parameter, benötigt so aber keine globalen Variablen.
Um Variablen per Referenz zu übergeben werden diese in Objekte (hier Listen) "verpackt".
Eine einfache Möglichkeit zur Optimierung wäre, einen Zweig vorzeitig zu verlassen, sobald das Maximalgewicht überschritten wird.

## Dynamische Programmierung
Die dynamische Programmierung nutzt eine Tabelle um erheblich schneller zu gleichfalls esten Lösung zu kommen.
Für jedes Item wird eine Zeile und für jedes Gewichsinkrement eine Spalte angelegt.
Nacheinander wird jedes Item erst einer Hilfszeile hinzugefügt.
Diese wird dafür um den Gewichtswert verschoben.
Anschließend wird verglichen, ob die Kombination der Hilfszeile einen größeren Wert hat als die letzte Kombination jeder Spalte, der bessere Wert wird überniommen.
Ist unten rechts der beste Wert gefunden, so wird die Tabele rückwärts betrachtet.
Das erste Auftreten eines Wertes in der Spalte zeigt das Hinzufügen eines Items an.
Dieses wird gespeichert, die Spalte um den Abzug des Gewichts nach links gewechselt und eine weitere Zeile aufgestiegen.
Dies wird so lange wiederholt bis ein Feld mit leerem Rucksack bzw. Wert 0 vorgefunden wird.
Liegen mehrere gleiche Werte nebeneinander kann auf gleich gute Kombinationen geschlossen werden.

## Laufzeit & Ergebnisse

### rosettacode Items

| Brute Force            	| Dynamisch              	|
|------------------------	|------------------------	|
| map                    	| map                    	|
| compass                	| compass                	|
| water                  	| water                  	|
| sandwich               	| sandwich               	|
| glucose                	| glucose                	|
| banana                 	| banana                 	|
| apple                  	| apple                  	|
| cheese                 	| cheese                 	|
| suntan cream           	| suntan cream           	|
| camera                 	| camera                 	|
| waterproof trousers    	| waterproof trousers    	|
| waterproof overclothes 	| waterproof overclothes 	|
| note-case              	| note-case              	|
| sunglasses             	| sunglasses             	|
| socks                  	| socks                  	|
|                           |                        	|
| Gewicht: 490           	| Gewicht: 490           	|
| Wert: 1130             	| Wert: 1130             	|
| time: 11.08s           	| time: 7.12ms           	|

-> Faktor 11,08s / 7,12ms = 1556 weniger Aufwand für das gleiche Ergebnis!

### knapsack01.data

| Items:         	|
|----------------	|
| 2              	|
| 3              	|
| 4              	|
| 5              	|
| 6              	|
| 7              	|
| 8              	|
| 13             	|
| 15             	|
| 17             	|
| 24             	|
| 25             	|
| 26             	|
| 27             	|
| 28             	|
| 30             	|
| 31             	|
| 32             	|
| 33             	|
| 36             	|
| 38             	|
| 40             	|
| 43             	|
| 44             	|
| 45             	|
| 48             	|
| 49             	|
| 50             	|
| 51             	|
| 52             	|
| 55             	|
| 57             	|
| 59             	|
| 61             	|
| 63             	|
| 64             	|
| 65             	|
| 69             	|
| 70             	|
| 71             	|
| 72             	|
| 73             	|
| 76             	|
| 78             	|
| 79             	|
| 80             	|
| 81             	|
| 82             	|
| 83             	|
| 86             	|
| 89             	|
| 93             	|
| 94             	|
| 95             	|
| 97             	|
| 99             	|
| 100            	|
| 101            	|
| 102            	|
| 103            	|
| 104            	|
| 105            	|
| 106            	|
| 107            	|
| 110            	|
| 112            	|
| 114            	|
| 117            	|
| 118            	|
| 119            	|
| 123            	|
| 124            	|
| 125            	|
| 126            	|
| 128            	|
| 129            	|
| 131            	|
| 133            	|
| 134            	|
| 136            	|
| 138            	|
| 141            	|
| 143            	|
| 145            	|
| 146            	|
| 147            	|
| 148            	|
| 151            	|
| 152            	|
| 155            	|
| 157            	|
| 158            	|
| 159            	|
| 161            	|
| 162            	|
| 163            	|
| 168            	|
| 169            	|
| 170            	|
| 172            	|
| 173            	|
| 175            	|
| 176            	|
| 179            	|
| 180            	|
| 181            	|
| 182            	|
| 183            	|
| 184            	|
| 185            	|
| 186            	|
| 187            	|
| 188            	|
| 192            	|
| 194            	|
| 195            	|
| 198            	|
| 199            	|
| 200            	|
|                	|
| Gewicht: 10000 	|
| Wert: 77876    	|
| time: 1.18s    	|