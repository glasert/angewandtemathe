import numpy as np

#config:
solve_brute_force = False
solve_dynamic     = True

# item:
#  name weight value
weight = 1
value = 2

def dynamic(items: list, capacity: int) -> list:
    print("Dynamic:")
    #Array anlegen
    num_items = len(items)
    array = np.zeros(shape=(num_items+1, capacity+1), dtype=int)
    
    #erstes item
    for i in range(items[0][weight], capacity+1):
        array[1][i] = items[0][value]
    
    # für  jedes weitere item
    for row, item in enumerate(items, start=1):
        #erstes item wurde schon hinzugefügt
        if row == 1:
            continue
        #erstelle eine Hilfszeile
        help_row = array[row-1].copy()
        #für jede Spalte
        for index in range(len(help_row)):
            #füge den item wert hinzu
            help_row[index] += item[value]
        #vergleiche die hilfszeile (verschoben um weight) mit der vorherigen Zeile
        #und trage den besten wert weiter
        for column in range(len(help_row)):
            if column < item[weight]:
                array[row][column] = array[row-1][column]
            if column >= item[weight]:
                array[row][column] = max(help_row[column-item[weight]], array[row-1][column])
    
    print(array)
    
    #backtracking
    loaded_items = []
    row = num_items
    column = capacity
    best_value = array[row, column]
    print("best value", best_value)
    while(column > 0):
        while(array[row-1, column] == best_value):
            row -= 1
        if (row <= 0):
            break
        item = items[row-1]
        loaded_items.append(item)
        row -= 1
        column -= item[weight]
        best_value -= item[value]
    
    
    #ausgabe
    loaded_items.reverse()
    end_weight = 0
    end_value = 0
    for item in loaded_items:
        print(item[0])
        end_weight += item[weight]
        end_value += item[value]
    print('Gewicht: ', end_weight)
    print('Wert: ', end_value)


def traverse(items: list, max_weight: int, bits: list, level: int, depth: int, best_value, best_bits: list) -> list:
    # Abbruchbedingung
    weight = 0
    value = 0
    for i, item in enumerate(items):
        if bits[i]:
            weight += item[1]
            value += item[2]
    
    # teste ob wert besser
    if value >= best_value[0] and weight < max_weight:
        best_value[0] = value
        best_bits[0] = bits.copy()
    
    # gehe nicht tiefer
    if level == depth:
        # Ausgabe des Bitmusters
        # print(bits)
        return
    
    # traversiere links
    bits[level] = 1
    traverse(items=items,
                max_weight=max_weight,
                bits=bits,
                level=level+1,
                depth=depth,
                best_value=best_value,
                best_bits=best_bits)
    
    # Aufräumen
    bits[level] = 0
    
    # traversiere rechts
    #bits[level] = 0 
    traverse(items=items,
                max_weight=max_weight,
                bits=bits,
                level=level+1,
                depth=depth,
                best_value=best_value,
                best_bits=best_bits)

def brute(items: list, capacity: int) -> list:
    print("Brute Force:")
    mybits = []
    best_bits = []
    for i in range(len(items)):
        mybits.append(0)
        
    best_bits.append([])
    best_bits[0] = mybits.copy()
    best_value = [0]
    
    traverse(items=items,
                max_weight=capacity,
                bits=mybits,
                level=0,
                depth=len(items),
                best_value=best_value,
                best_bits=best_bits)
    
    print(best_bits)
    weight = 0
    for i, item in enumerate(items):
        if best_bits[0][i]:
            weight += item[1]
            print(item[0])
    print('Gewicht: ', weight)
    print('Wert: ', best_value[0])

def parse_data_file(path: str) -> list:
    items = []
    with open(file=path, mode='r') as file:
        for line in file:
            entries = line.split('\t')
            if entries[0] != 'i':
                items.append((str(entries[0]), int(entries[1]), int(entries[2])))
    return items

def get_items(file_path: str = None) -> list:
    if file_path == None:
        return [
            ('map', 9, 150), ('compass', 13, 35), ('water', 153, 200), ('sandwich', 50, 160),
            ('glucose', 15, 60), ('tin', 68, 45), ('banana', 27, 60), ('apple', 39, 40),
            ('cheese', 23, 30), ('beer', 52, 10), ('suntan cream', 11, 70), ('camera', 32, 30),
            ('t-shirt', 24, 15), ('trousers', 48, 10), ('umbrella', 73, 40),
            ('waterproof trousers', 42, 70), ('waterproof overclothes', 43, 75),
            ('note-case', 22, 80), ('sunglasses', 7, 20), ('towel', 18, 12), ('socks', 4, 50),
            ('book', 30, 10) # courtesy https://rosettacode.org/wiki/Knapsack_problem/0-1
        ]
    else:
        return parse_data_file(file_path)

if __name__ == "__main__":
    
    if solve_brute_force:
        items = get_items()
        brute(items, 500)
    
    if solve_dynamic:
        items = get_items("Blatt2/knapsack01.data")
        dynamic(items, 10000)
        #items = get_items()
        #dynamic(items, 500)
