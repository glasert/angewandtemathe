import numpy as np
from copy import deepcopy
import chess
import chess.svg
from IPython.display import Image
from IPython.display import SVG

class Board:
    blocked = 0
    allowed = 1
    queen = 2
    
    def __init__(self, x: int, y: int):
        self.board = np.ones((x, y), dtype=int)
        self.dimX = x
        self.dimY = y
        
    def copy(self):
        return deepcopy(self)
    
    def toConsole(self) -> None:
        for y in range(self.dimY):
            line = ""
            for x in range(self.dimX):
                if self.board[x, y] == self.blocked:
                    line += "X"
                if self.board[x, y] == self.allowed:
                    line += "O"
                if self.board[x, y] == self.queen:
                    line += "@"
            print(line)
        print("")
    
    def print(self) -> None:
        self.toConsole()
    
    def get_allowed_fields(self):
        allowed_fields = []
        for x in range(self.dimX):
            for y in range(self.dimY):
                if self.board[x, y] == self.allowed:
                    allowed_fields.append((x, y))
        return allowed_fields
    
    def get_queen_fields(self):
        queen_fields = []
        for x in range(self.dimX):
            for y in range(self.dimY):
                if self.board[x, y] == self.allowed:
                    queen_fields.append((x, y))
        return queen_fields
    
    def put_queen(self, x: int, y: int) -> bool:
        if self.board[x, y] == self.allowed:
            for _x in range(self.dimX):
                self.board[_x, y] = self.blocked
            for _y in range(self.dimY):
                self.board[x, _y] = self.blocked

            _x = x
            _y = y
            while _x > 0 and _y > 0:
                _y -= 1
                _x -= 1
                self.board[_x, _y] = self.blocked
            
            _x = x
            _y = y
            while _x < self.dimX-1 and _y < self.dimY-1:
                _y += 1
                _x += 1
                self.board[_x, _y] = self.blocked
            self.board[x, y] = self.queen
            return True
        else:
            return False
        
    def display_chess_board(self) -> None:
        string = ""
        for row in range(self.dimY):
            spaces = 0
            for column in range(self.dimX):
                if self.board[column, row] == self.queen:
                    if spaces > 0:
                        string += str(spaces)
                    string += "q"
                    spaces = 0
                else:
                    spaces += 1
            if spaces > 0:
                string += str(spaces)
            if row < self.dimY-1:
                string += "/"
        
        board = chess.Board(string)
        boardsvg = chess.svg.board(board, size=350)
        outputfile = open('temp.svg', "w")
        outputfile.write(boardsvg)
        outputfile.close()
        display(SVG("temp.svg"))


if __name__ == "__main__":
    board = Board(8, 8)
    num_queens = 8
    solutions = []
    
    saved_boards = []
    iters = 0
    while board.get_allowed_fields().__len__() > 0:
        board.put_queen(board.get_allowed_fields()[0][0],
                        board.get_allowed_fields()[0][1])
        iters += 1
        board.toConsole()
        saved_boards.append(board.copy())
    print("Queens: " + str(iters))
    board.display_chess_board()


    def recurse(board: Board, branch: int, depth: int) -> None:
        # Anker:
        
        
        oldBoard = board
        workboard = board.copy()
        # traversiere links
        
        