import matplotlib.pyplot as plt
from math import sqrt
from random import randint
from itertools import permutations

# config:
## use tsp01.data?
from_file = False
## number of nodes when false:
num_nodes = 10
## brute force or nearest neighbout?
brute_force = True
## apply 2-opt?
optimize = False
## plot better routes during calcutation
do_plot = True
## duration of pause between plots (for better visibility)
pause = 0

class Node:
    x: int
    y: int
    name: str
    def __init__(self, x: int, y: int, name: str = ""):
        self.x = x
        self.y = y
        self.name = name
    
    def plot(this):
        plt.plot(this.x, this.y, 'ro', label=this.name)
    
    def __str__(self) -> str:
        if self.name != "":
            return "x: " + str(self.x) + "\ty: " + str(self.y) + "\tname: " + self.name
        else:
            return "x: " + str(self.x) + "\ty: " + str(self.y)

def distance(A: Node, B: Node) -> float:
    return sqrt((A.x - B.x)**2 + (A.y - B.y)**2)

def total_distance(route: list, circle: bool = False) -> float:
    total_distance = 0
    for i in range(len(route)):
        if i == len(route)-1:
            if circle:
                total_distance += distance(route[i], route[0])
        else:
            total_distance += distance(route[i], route[i+1])
    return total_distance

def plot_path(A: Node, B: Node):
    plt.plot([A.x, B.x], [A.y, B.y], 'go-')

def plot_route(route: list, nodes: list = [], circle: bool = False) -> None:
    plt.close("all")
    for i in range(len(route)):
        if i == len(route) - 1:
            if circle:
                plot_path(A=route[i], B=route[0])
        else:
            plot_path(A=route[i], B=route[i+1])
    for node in nodes:
        node.plot()
    plt.show()
    plt.pause(pause)


def brute(nodes: list) -> list:
    if len(nodes) <= 1:
        return
    best_route = nodes
    best_route_length = total_distance(nodes, circle=True)
    if do_plot:
        plot_route(best_route, circle=True)
    for i, route in enumerate(permutations(nodes, len(nodes))):
        route_length = total_distance(route, circle=True)
        if route_length < best_route_length:
            best_route = route
            best_route_length = route_length
            print("Found shorter Route with length " + str(best_route_length) + " after " +  str(i) + " iterations.")
            if do_plot:
                plot_route(best_route, circle=True)
    print(str(i+1) + " iterations in total")
    return best_route


def get_closest_node(current_node: Node, nodes: list) -> Node:
    if len(nodes) == 0:
        return
    closest_node = nodes[0]
    closest_distance = distance(current_node, nodes[0])
    for node in nodes:
        dist = distance(current_node, node)
        if dist < closest_distance:
            closest_node = node
            closest_distance = dist
    return closest_node

def nearest_neighbour(_nodes: list) -> list:
    remaining_nodes = _nodes.copy()
    route = []
    if len(remaining_nodes) == 0:
        return route
    route.append(remaining_nodes[0])
    last_node = remaining_nodes[0]
    remaining_nodes.remove(last_node)
    while(len(remaining_nodes) > 0):
        next_node = get_closest_node(last_node, remaining_nodes)
        remaining_nodes.remove(next_node)
        route.append(next_node)
        last_node = next_node
        if do_plot:
            plot_route(route, _nodes)
    return route


#TODO Zuweisungen brechen die Kette!
def optimize_2opt(_route: list) -> list:
    route = _route.copy()
    
    for i in range(len(route)-1):
        print("My 2-opt is broken!")
        print("TODO Zuweisungen brechen die Kette!")
        a = route[i]
        b = route[i+1]
        ab  = distance(a, b)
        
        for j in range(2, len(route)-1):
            swapped = False
            c = route[j]
            d = route[j+1]
            
            cd = distance(c, d)
            abcd = ab + cd
            
            ac = distance(a, c)
            db = distance(d, b)
            acdb = ac + db
            
            ad = distance(a, d)
            cb = distance(c, b)
            adcb = ad + cb
            
            if adcb < acdb:
                if adcb < abcd:
                    #adcb
                    #swap d & b
                    route[i+1] = d
                    route[j+1] = b
                    #abcd
                    swapped = True
            else:
                if acdb < abcd:
                    #acdb
                    #swap c & b
                    route[j] = b
                    route[i+1] = c
                    #abdc
                    #swap c & d
                    route[j] = d
                    route[j+1]  = c
                    #abcd
                    swapped = True
            if swapped:
                break
    return route

def parse_data_file(path: str) -> list:
    nodes = []
    with open(file=path, mode='r') as file:
        for line in file:
            coordinates = line.split('\t')
            new_node = Node(x=float(coordinates[0]), y=float(coordinates[1]))
            nodes.append(new_node)
    return nodes

if __name__ == "__main__":
    nodes = []
    if from_file:
        nodes = parse_data_file("Blatt1/tsp01.data")
    else:
        for i in range(num_nodes):
            nodes.append(Node(x=randint(0, 100), y=randint(0, 100), name=str(hex(i))))

    plt.ion()
    
    if brute_force:
        best_route = brute(nodes)
    else:
        best_route = nearest_neighbour(nodes)
        if optimize:
            best_route = optimize_2opt(best_route)
    
    
    plot_route(best_route, nodes, circle=True)
    plt.show(block=True)
